import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:intl/intl.dart' show DateFormat;

void main() => runApp(MaterialApp(
      home: Calendar(),
      debugShowCheckedModeBanner: false,
    ));

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  DateTime _currentDate = DateTime(2019, 9, 16);

  String _currentMonth = '';

  static Widget _eventIcon = new Container(
    decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.red, width: 2.0)),
  );

  EventList<Event> _markedDateMap = new EventList<Event>(
    events: {
      new DateTime(2019, 9, 13): [
        new Event(
          date: new DateTime(2019, 9, 13),
          title: 'Event 1',
          icon: _eventIcon,
        ),
      ],
      new DateTime(2019, 9, 13): [
        new Event(
          date: new DateTime(2019, 9, 13),
          title: 'Event 1',
          icon: _eventIcon,
        ),
      ],
      new DateTime(2019, 9, 11): [
        new Event(
          date: new DateTime(2019, 9, 11),
          title: 'Event 1',
          icon: _eventIcon,
        ),
      ],
      new DateTime(2019, 9, 15): [
        new Event(
          date: new DateTime(2019, 9, 15),
          title: 'Event 1',
          icon: _eventIcon,
        ),
      ],
    },
  );

  CalendarCarousel _calendarCarouselNoHeader;

  @override
  void initState() {
    /// Add more events to _markedDateMap EventList
    _markedDateMap.add(
        new DateTime(2019, 9, 3),
        new Event(
          date: new DateTime(2019, 9, 3),
          title: 'Event',
          icon: _eventIcon,
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _calendarCarouselNoHeader = CalendarCarousel<Event>(
      defaultMarkedDateWidgetColor: Colors.redAccent,
      markedDateIconBorderColor: Colors.red,

      iconColor: Colors.grey,
      todayBorderColor: Colors.teal[600],
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate = date);
        events.forEach((event) => print(event.title));
      },
      weekendTextStyle: TextStyle(
        color: Colors.red,
      ),
      thisMonthDayBorderColor: Colors.grey,
      weekFormat: false,
      markedDatesMap: _markedDateMap,
      height: 400.0,
      selectedDateTime: _currentDate,
//      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateShowIcon: false,
//      markedDateIconMaxShown: 2,
      markedDateMoreShowTotal: false,
      // null for not showing hidden events indicator
//      showHeader: false,
//      markedDateIconBuilder: (event) {
//        return event.icon;
//      },
      todayTextStyle: TextStyle(
        color: Colors.blue,
      ),
      todayButtonColor: Colors.yellow,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
      ),
//      minSelectedDate: _currentDate,
//      maxSelectedDate: _currentDate.add(Duration(days: 60)),
//      inactiveDateColor: Colors.black12,
      onCalendarChanged: (DateTime date) {
        this.setState(() => _currentMonth = DateFormat.yMMM().format(date));
      },
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal[600],
        elevation: 0,
      ),
      endDrawer: Drawer(),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(Icons.arrow_drop_down),
                    Text(
                      "الحضور والغياب",
                      style: TextStyle(color: Colors.grey[800], fontSize: 13),
                    ),
                  ],
                ),

                //الاسم
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "حمزة حسين عبدالرحمن",
                          style: TextStyle(
                              color: Colors.teal[600],
                              fontWeight: FontWeight.bold),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 4,
                                ),
                                Text(
                                  "1440-2019",
                                  style: TextStyle(color: Colors.grey[800]),
                                ),
                                Container(
                                  height: 0.75,
                                  width: 70,
                                  color: Colors.black87,
                                )
                              ],
                            ),
                            SizedBox(width: 1),
                            Column(
                              children: <Widget>[
                                Text("التقويم",
                                    style: TextStyle(color: Colors.grey[800])),
                                Container(
                                  height: 0.75,
                                  width: 40,
                                  color: Colors.black87,
                                )
                              ],
                            ),
                            Icon(
                              Icons.calendar_today,
                              color: Colors.teal[600],
                              size: 22,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                          ],
                        )
                      ],
                    ),
                    //Person Icon
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black87),
                          borderRadius: BorderRadius.circular(25)),
                      child: Icon(
                        Icons.person,
                        color: Colors.grey,
                        size: 42,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            _calendarCarouselNoHeader,

            SizedBox(height: 8),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[

                Row(
                  children: <Widget>[
                    Text("غياب",style: TextStyle(color: Colors.grey[600],fontSize: 11),),
                    SizedBox(width: 5),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.grey
                      ),
                      height: 7.0,
                      width: 7.0,
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("التقدم في المركز",style: TextStyle(color: Colors.grey[600],fontSize: 11),),
                    SizedBox(width: 5),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.orangeAccent
                      ),
                      height: 7.0,
                      width: 7.0,
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("يحتاج إلى تطوير",style: TextStyle(color: Colors.grey[600],fontSize: 11),),
                    SizedBox(width: 5),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.red
                      ),
                      height: 7.0,
                      width: 7.0,
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("تم إنجاز المقرر بتفوق",style: TextStyle(color: Colors.grey[600],fontSize: 11),),
                    SizedBox(width: 5),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.orange
                      ),
                      height: 7.0,
                      width: 7.0,
                    )
                  ],
                ),
              ],
            ),
            SizedBox(height: 5)
          ],
        ),
      ),
    );
  }
}
